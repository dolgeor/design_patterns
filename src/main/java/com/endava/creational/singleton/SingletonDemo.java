package com.endava.creational.singleton;

public class SingletonDemo {

    public static void main(String[] args) {
        Connection c = Connection.getConnection();
        System.out.println(c);
        c = Connection.getConnection();
        System.out.println(c);
    }
}
