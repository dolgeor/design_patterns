package com.endava.creational.factory;

import com.endava.creational.factory.cars.Honda;
import com.endava.creational.factory.cars.Mercedes;
import com.endava.creational.factory.cars.Toyota;

public class CarFactory {

    public static Car getCar(Brand brand) {
        switch (brand) {
            case HONDA:
                return new Honda();
            case TOYOTA:
                return new Toyota();
            case MERCEDES:
                return new Mercedes();
            default:
                throw new IllegalArgumentException("Unknown brand");
        }
    }
}
