package com.endava.creational.factory;

public enum Brand {
    HONDA,
    MERCEDES,
    TOYOTA;
}
