package com.endava.creational.factory.cars;

import com.endava.creational.factory.Car;

public class Toyota implements Car {

    @Override
    public void drive() {
        System.out.println("Driving Lexus");
    }
}