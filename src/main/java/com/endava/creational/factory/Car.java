package com.endava.creational.factory;

public interface Car {
    void drive();
}
