package com.endava.creational.factory;

import static com.endava.creational.factory.Brand.HONDA;
import static com.endava.creational.factory.Brand.MERCEDES;

public class FactoryMethodDemo {

    public static void main(String[] args) {
        CarFactory.getCar(HONDA).drive();
        CarFactory.getCar(MERCEDES).drive();
        CarFactory.getCar(HONDA).drive();
    }
}
