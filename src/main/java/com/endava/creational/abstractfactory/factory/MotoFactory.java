package com.endava.creational.abstractfactory.factory;

import com.endava.creational.abstractfactory.Brand;
import com.endava.creational.abstractfactory.Vehicle;
import com.endava.creational.abstractfactory.VehicleFactory;
import com.endava.creational.abstractfactory.moto.Bmw;
import com.endava.creational.abstractfactory.moto.Honda;

public class MotoFactory implements VehicleFactory {

    @Override
    public Vehicle getVehicle(final Brand brand) {
        switch (brand) {
            case HONDA:
                return new Honda();
            case BMW:
                return new Bmw();
            default:
                throw new IllegalArgumentException("Unknown brand");
        }
    }
}
