package com.endava.creational.abstractfactory.factory;

import com.endava.creational.abstractfactory.Brand;
import com.endava.creational.abstractfactory.Vehicle;
import com.endava.creational.abstractfactory.VehicleFactory;
import com.endava.creational.abstractfactory.car.Bmw;
import com.endava.creational.abstractfactory.car.Honda;

public class CarFactory implements VehicleFactory {

    @Override
    public Vehicle getVehicle(final Brand brand) {
        switch (brand) {
            case HONDA:
                return new Honda();
            case BMW:
                return new Bmw();
            default:
                throw new IllegalArgumentException("Unknown brand");
        }
    }
}
