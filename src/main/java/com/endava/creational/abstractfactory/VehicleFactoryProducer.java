package com.endava.creational.abstractfactory;

import com.endava.creational.abstractfactory.factory.CarFactory;
import com.endava.creational.abstractfactory.factory.MotoFactory;

public class VehicleFactoryProducer {

    public static VehicleFactory getVehicleFactory(final VehicleType type) {
        switch (type) {
            case CAR:
                return new CarFactory();
            case MOTO:
                return new MotoFactory();
            default:
                throw new IllegalArgumentException("Unknown vehicle type");
        }
    }
}
