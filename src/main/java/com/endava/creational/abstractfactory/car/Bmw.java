package com.endava.creational.abstractfactory.car;

public class Bmw implements Car {

    @Override
    public void drive() {
        System.out.println("Driving Bmw Car");
    }
}