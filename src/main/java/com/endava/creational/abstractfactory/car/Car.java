package com.endava.creational.abstractfactory.car;

import com.endava.creational.abstractfactory.Vehicle;

public interface Car extends Vehicle {

}
