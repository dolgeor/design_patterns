package com.endava.creational.abstractfactory.car;

public class Honda implements Car {

    @Override
    public void drive() {
        System.out.println("Driving Honda Car");
    }
}