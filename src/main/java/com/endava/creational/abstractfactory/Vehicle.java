package com.endava.creational.abstractfactory;

public interface Vehicle {
    void drive();
}
