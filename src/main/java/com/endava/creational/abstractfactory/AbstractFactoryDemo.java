package com.endava.creational.abstractfactory;

import static com.endava.creational.abstractfactory.Brand.BMW;
import static com.endava.creational.abstractfactory.Brand.HONDA;
import static com.endava.creational.abstractfactory.VehicleType.CAR;
import static com.endava.creational.abstractfactory.VehicleType.MOTO;

public class AbstractFactoryDemo {

    public static void main(String[] args) {
        VehicleFactoryProducer.getVehicleFactory(MOTO).getVehicle(HONDA).drive();
        VehicleFactoryProducer.getVehicleFactory(MOTO).getVehicle(BMW).drive();

        VehicleFactoryProducer.getVehicleFactory(CAR).getVehicle(HONDA).drive();
        VehicleFactoryProducer.getVehicleFactory(CAR).getVehicle(BMW).drive();
    }
}
