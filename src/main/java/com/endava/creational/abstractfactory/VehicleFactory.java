package com.endava.creational.abstractfactory;

public interface VehicleFactory {

    Vehicle getVehicle(Brand brand);
}
