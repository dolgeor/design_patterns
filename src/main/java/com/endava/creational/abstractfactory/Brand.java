package com.endava.creational.abstractfactory;

public enum Brand {
    HONDA,
    BMW;
}
