package com.endava.creational.abstractfactory.moto;

public class Honda implements Moto {

    @Override
    public void drive() {
        System.out.println("Driving Honda Moto");
    }
}