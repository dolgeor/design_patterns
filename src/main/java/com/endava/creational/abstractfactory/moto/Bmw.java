package com.endava.creational.abstractfactory.moto;

public class Bmw implements Moto {

    @Override
    public void drive() {
        System.out.println("Driving Bmw Moto");
    }
}