package com.endava.creational.abstractfactory;

public enum VehicleType {
    CAR,
    MOTO;
}
