package com.endava.creational.prototype;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Student {

    private int age;

    private String name;

    public Student clone() {
        return new Student(age, name);
    }

    @Override
    public String toString() {
        return "Student{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", '" + super.toString() + '\'' +
                '}';
    }
}