package com.endava.creational.prototype;

public class PrototypeDemo {

    public static void main(String[] args) {
        Student s = new Student(21, "Jony");
        System.out.println(s);
        System.out.println(s.clone());
        System.out.println(s.clone());
        System.out.println(s.clone());
        System.out.println(s);
    }
}
