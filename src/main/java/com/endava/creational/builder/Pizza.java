package com.endava.creational.builder;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Pizza {

    private boolean tomatoSauce;

    private boolean onions;

    private String mushrooms;

    private String peppers;

    private String cheese;

    private String salami;

    private String meat;

    private String fish;

    @Override
    public String toString() {
        final StringBuilder  sb = new StringBuilder("Pizza:\n");
        if (tomatoSauce)     sb.append("\n\ttomatoSauce");
        if (onions)          sb.append("\n\tonions");
        if (mushrooms!=null) sb.append("\n\tmushrooms='").append(mushrooms).append('\'');
        if (peppers!=null)   sb.append("\n\tpeppers='").append(peppers).append('\'');
        if (cheese!=null)    sb.append("\n\tcheese='").append(cheese).append('\'');
        if (salami!=null)    sb.append("\n\tsalami='").append(salami).append('\'');
        if (meat!=null)      sb.append("\n\tmeat='").append(meat).append('\'');
        if (fish!=null)      sb.append("\n\tfish='").append(fish).append('\'');
        return sb.toString();
    }
}
