package com.endava.creational.builder;

public class BuilderDemo {

    public static void main(String[] args) {
        final Pizza pizza =
                new PizzaBuilder()
                        .addTomatoSauce()
                        .addCheese("Mozzarella")
                        .addSalami("Salami")
                        .addMushrooms("Mushrooms")
                        .getPizza();
        System.out.println(pizza);
    }
}
