package com.endava.creational.builder;

public class PizzaBuilder {

    private boolean tomatoSauce;

    private boolean onions;

    private String mushrooms;

    private String peppers;

    private String cheese;

    private String salami;

    private String meat;

    private String fish;

    public PizzaBuilder() {
    }

    public PizzaBuilder addTomatoSauce() {
        this.tomatoSauce = true;
        return this;
    }

    public PizzaBuilder addOnions() {
        this.onions = true;
        return this;
    }

    public PizzaBuilder addMushrooms(String mushrooms) {
        this.mushrooms = mushrooms;
        return this;
    }

    public PizzaBuilder addPeppers(final String peppers) {
        this.peppers = peppers;
        return this;
    }

    public PizzaBuilder addCheese(final String cheese) {
        this.cheese = cheese;
        return this;
    }

    public PizzaBuilder addSalami(final String salami) {
        this.salami = salami;
        return this;
    }

    public PizzaBuilder addMeat(final String meat) {
        this.meat = meat;
        return this;
    }

    public PizzaBuilder addFish(final String fish) {
        this.fish = fish;
        return this;
    }

    public Pizza getPizza() {
        return new Pizza(tomatoSauce, onions, mushrooms, peppers, cheese, salami, meat, fish);
    }

}
