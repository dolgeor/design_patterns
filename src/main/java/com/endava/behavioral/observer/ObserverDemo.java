package com.endava.behavioral.observer;

import java.util.ArrayList;

public class ObserverDemo {

    public static void main(String[] args) {
        YouTubeChannel channel = new YouTubeChannel(new ArrayList<>());

        final Subscriber jon = new Subscriber("Jon");
        final Subscriber pit = new Subscriber("Pit");
        final Subscriber george = new Subscriber("George");

        channel.addSubscriber(jon);
        channel.addSubscriber(pit);
        channel.addSubscriber(george);

        channel.publishNewVideo("Solid tutorial");
    }
}
