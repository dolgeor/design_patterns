package com.endava.behavioral.observer;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Subscriber {

    private String name;

    public void notify(final String notification) {
        System.out.println(notification);
    }
}
