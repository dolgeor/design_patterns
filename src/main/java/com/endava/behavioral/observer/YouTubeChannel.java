package com.endava.behavioral.observer;

import java.util.List;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class YouTubeChannel {

    private List<Subscriber> subscribers;

    public void addSubscriber(final Subscriber subscriber) {
        subscribers.add(subscriber);
    }

    public void removeSubscriber(final Subscriber subscriber) {
        subscribers.remove(subscriber);
    }

    public void publishNewVideo(final String title) {
        System.out.printf("YouTube Channel publishes new video : %s %n%n", title);
        subscribers.forEach(s -> s.notify(String.format("Hi %6s, you can enjoy new video on our channel '%s' :)", s.getName(), title)));
    }
}
