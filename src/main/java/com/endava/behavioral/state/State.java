package com.endava.behavioral.state;

public enum State {
    LOCKED, UNLOCKED
}
