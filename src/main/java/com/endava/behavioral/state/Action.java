package com.endava.behavioral.state;

public enum Action {
    COIN, PUSH
}
