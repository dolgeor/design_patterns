package com.endava.behavioral.state;

import static com.endava.behavioral.state.Action.COIN;
import static com.endava.behavioral.state.Action.PUSH;
import static com.endava.behavioral.state.State.LOCKED;

public class StateDemo {

    public static void main(String[] args) {
        final Turnstile turnstile = new Turnstile(LOCKED);
        System.out.printf("Initial: %s%n%n", turnstile.getCurrentState());

        System.out.printf("Action:  %s%n", PUSH);
        turnstile.processAction(PUSH);
        System.out.printf("-> %s%n%n" , turnstile.getCurrentState());

        System.out.printf("Action:  %s%n", COIN);
        turnstile.processAction(COIN);
        System.out.printf("-> %s%n%n" , turnstile.getCurrentState());

        System.out.printf("Action:  %s%n", PUSH);
        turnstile.processAction(PUSH);
        System.out.printf("-> %s%n%n", turnstile.getCurrentState());
    }

}
