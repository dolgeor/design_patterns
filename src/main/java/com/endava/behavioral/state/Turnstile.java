package com.endava.behavioral.state;

import static com.endava.behavioral.state.Action.COIN;
import static com.endava.behavioral.state.Action.PUSH;
import static com.endava.behavioral.state.State.LOCKED;
import static com.endava.behavioral.state.State.UNLOCKED;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Turnstile {

    private State currentState;

    public void processAction(final Action action) {
        if (currentState == LOCKED && action == COIN) {
            currentState = UNLOCKED;
        } else if (currentState == UNLOCKED && action == PUSH) {
            currentState = LOCKED;
        }
    }
}
