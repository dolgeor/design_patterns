package com.endava.behavioral.chain;

import java.time.LocalDate;

public class ATMDemo {

    public static void main(String[] args) {
        final Validator withdrawValidator = getWithdrawValidator();
        final WithdrawRequest request = new WithdrawRequest("1111", LocalDate.of(2027, 4, 17), 200);

        withdrawValidator.validate(request);

        System.out.printf("You can get your %d$", request.getAmount());
    }

    private static Validator getWithdrawValidator() {
        final PinValidator pinValidator = new PinValidator();
        final ExpirationDateValidator expirationDateValidator = new ExpirationDateValidator();
        final AvailableCashValidator cashValidator = new AvailableCashValidator();

        pinValidator.setNextValidator(expirationDateValidator);
        expirationDateValidator.setNextValidator(cashValidator);
        return pinValidator;
    }
}
