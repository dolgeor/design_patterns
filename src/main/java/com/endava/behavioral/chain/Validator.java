package com.endava.behavioral.chain;

public interface Validator {

    void validate(final WithdrawRequest request);
}
