package com.endava.behavioral.chain;

import lombok.Setter;

@Setter
public class AvailableCashValidator implements Validator {

    private static final Integer CASH = 1_000;

    private Validator nextValidator;

    @Override
    public void validate(final WithdrawRequest request) {
        if (casWithdraw(request)) {
            throw new IllegalArgumentException("Sorry, you want too much !!!");
        }
        if (nextValidator != null) {
            nextValidator.validate(request);
        }
    }

    private boolean casWithdraw(final WithdrawRequest request) {
        return CASH < request.getAmount();
    }
}
