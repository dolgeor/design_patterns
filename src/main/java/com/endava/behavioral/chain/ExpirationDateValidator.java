package com.endava.behavioral.chain;

import java.time.LocalDate;

import lombok.Setter;

@Setter
public class ExpirationDateValidator implements Validator {

    private Validator nextValidator;

    @Override
    public void validate(final WithdrawRequest request) {
        if (hasExpiredDate(request)) {
            throw new IllegalArgumentException("Sorry, you card has expired !!!");
        }
        if (nextValidator != null) {
            nextValidator.validate(request);
        }
    }

    private boolean hasExpiredDate(final WithdrawRequest request) {
        return LocalDate.now().isAfter(request.getExpirationDate());
    }
}
