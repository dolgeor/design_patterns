package com.endava.behavioral.chain;

import lombok.Setter;

@Setter
public class PinValidator implements Validator {

    private static final String PIN = "1111";

    private Validator nextValidator;

    @Override
    public void validate(final WithdrawRequest request) {
        if (hasInvalidPin(request)) {
            throw new IllegalArgumentException("Sorry, you entered invalid PIN !!!");
        }
        if (nextValidator != null) {
            nextValidator.validate(request);
        }
    }

    private boolean hasInvalidPin(final WithdrawRequest request) {
        return !PIN.equals(request.getPin());
    }
}
