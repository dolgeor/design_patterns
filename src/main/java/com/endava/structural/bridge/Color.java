package com.endava.structural.bridge;

public interface Color {

    void applyColor();
}
