package com.endava.structural.bridge;

public class BridgeDemo {

    public static void main(String[] args) {
        Shape triangle = new Triangle(new RedColor());
        triangle.applyColor();

        Shape square = new Square(new GreenColor());
        square.applyColor();
    }
}
