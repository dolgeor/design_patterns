package com.endava.structural.decorator;

public class DecoratorDemo {

    public static void main(String[] args) {
        Coffee c = new SimpleCoffee();
        c.printInfo();

        c = new WithMilk(c);
        c.printInfo();

        c = new WithSprinkles(c);
        c.printInfo();
    }
}
