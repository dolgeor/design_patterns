package com.endava.structural.decorator;

public interface Coffee {

    double getCost();

    String getIngredients();

    public default void printInfo() {
        System.out.println("Cost: " + getCost() + "; Ingredients: " + getIngredients());
    }
}
