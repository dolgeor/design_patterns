package com.endava.structural.proxy;

public class ProxyDemo {

    public static void main(String[] args) {
        Project project1 = new ProxyProject("ihu");

        Project project2 = new ProxyProject("lololo");

        project2.run();
        project2.run();
    }
}
