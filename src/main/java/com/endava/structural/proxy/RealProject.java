package com.endava.structural.proxy;

public class RealProject implements Project {

    private String name;

    public RealProject(final String name) {
        this.name = name;
        download();
    }

    private void download() {
        System.out.println("Downloading " + name);
    }

    @Override
    public void run() {
        System.out.println("Running " + name);
    }
}
