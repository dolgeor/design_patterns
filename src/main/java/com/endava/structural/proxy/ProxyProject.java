package com.endava.structural.proxy;

public class ProxyProject implements Project {

    private String name;

    private RealProject realProject;

    public ProxyProject(final String name) {
        this.name = name;
    }

    @Override
    public void run() {
        if (realProject == null) {
            realProject = new RealProject(name);
        }
        realProject.run();
    }
}
