package com.endava.structural.proxy;

public interface Project {

    void run();
}
