package com.endava.structural.adapter;

import java.util.stream.Stream;

public class AdapterDemo {

    public static void main(String[] args) {
        Stream.of(
                new RectangleAdapter(new Rectangle()),
                new LineAdapter(new Line())
        )
                .forEach(shape -> shape.draw(10, 30, 20, 60));
    }
}
